#include "String.h"

typedef struct s_StringPriv
{
	char	*str;
	size_t	len;
	size_t	len_max;
	bool	is_const;
}			t_StringPriv;

void	string_append(String *string, char *str_a)
{
	size_t	len;
	size_t	len_left;
	int		len_string;
	char	*new;

	if (str_a == NULL || object_critical(string, "String"))
		return ;
	if (string->priv->is_const == true)
		string_set(string, string->priv->str);
	len = ft_strlen(str_a);
	len_left = string->priv->len_max - string->priv->len;
	if (len < len_left)
	{
		ft_memcpy(string->priv->str + string->priv->len, str_a, len + 1);
		string->priv->len += len;
	}
	else
	{
		len_string = string->priv->len;
		string->priv->len_max = len_string + (len * 2) + 512;
		new = malloc(sizeof(char) * string->priv->len_max);
		ft_memcpy(new, string->priv->str, string->priv->len + 1);
		free(string->priv->str);
		string->priv->str = new;
		ft_memcpy(string->priv->str + string->priv->len, str_a, len);
		string->priv->len += len;
	}
}

void string_set (String* string, const char *str) {
	size_t len;

	if (str == NULL || object_critical(string, "String"))
		return ;
	len = ft_strlen(str);
	if (string->priv->is_const == true || len > string->priv->len_max) {
		if (string->priv->is_const == false)
			free(string->priv->str);
		string->priv->len_max = len + 128;
		string->priv->str = malloc(sizeof(char) * (string->priv->len_max + 1));
		string->priv->is_const = false;
	}
	ft_memcpy (string->priv->str, str, len + 1);
}

void	string_up(String *string)
{
	size_t	magik_number;
	int		len;
	char	*s;
	size_t	*ptr;

	if (object_critical(string, "String"))
		return ;
	if (string->priv->is_const == true)
		string_set(string, string->priv->str);
	len = string->priv->len;
	s = string->priv->str;
	ptr = (size_t *)s;
	while (len >= 8)
	{
		magik_number = 0;
		if (('a' <= s[0] && 'z' >= s[0]))
			magik_number += 0x20;
		if (('a' <= s[1] && 'z' >= s[1]))
			magik_number += 0x2000;
		if (('a' <= s[2] && 'z' >= s[2]))
			magik_number += 0x200000;
		if (('a' <= s[3] && 'z' >= s[3]))
			magik_number += 0x20000000;
		if (('a' <= s[4] && 'z' >= s[4]))
			magik_number += 0x2000000000;
		if (('a' <= s[5] && 'z' >= s[5]))
			magik_number += 0x200000000000;
		if (('a' <= s[6] && 'z' >= s[6]))
			magik_number += 0x20000000000000;
		if (('a' <= s[7] && 'z' >= s[7]))
			magik_number += 0x2000000000000000;
		s += 8;
		*ptr -= magik_number;
		++ptr;
		len -= 8;
	}
	while (len > 0)
	{
		if (('a' <= *s && 'z' >= *s))
			*s -= 0x20;
		++s;
		--len;
	}
}

void	string_down(String *string)
{
	size_t	magik_number;
	int		len;
	char	*s;
	size_t	*ptr;

	if (object_critical(string, "String"))
		return ;
	if (string->priv->is_const == true)
		string_set(string, string->priv->str);
	len = string->priv->len;
	s = string->priv->str;
	ptr = (size_t *)s;
	while (len >= 8)
	{
		magik_number = 0;
		if (('A' <= s[0] && 'Z' >= s[0]))
			magik_number += 0x20;
		if (('A' <= s[1] && 'Z' >= s[1]))
			magik_number += 0x2000;
		if (('A' <= s[2] && 'Z' >= s[2]))
			magik_number += 0x200000;
		if (('A' <= s[3] && 'Z' >= s[3]))
			magik_number += 0x20000000;
		if (('A' <= s[4] && 'Z' >= s[4]))
			magik_number += 0x2000000000;
		if (('A' <= s[5] && 'Z' >= s[5]))
			magik_number += 0x200000000000;
		if (('A' <= s[6] && 'Z' >= s[6]))
			magik_number += 0x20000000000000;
		if (('A' <= s[7] && 'Z' >= s[7]))
			magik_number += 0x2000000000000000;
		s += 8;
		*ptr += magik_number;
		++ptr;
		len -= 8;
	}
	while (len > 0)
	{
		if (('A' <= *s && 'Z' >= *s))
			*s += 0x20;
		++s;
		--len;
	}
}

bool string_contains(String *string, char* str)
{
	return (ft_strstr(string->priv->str, str) != NULL);
}

// remove leading and trailing whitespaces
void string_strip (String* string) {
	size_t len;
	char* str;
	size_t i;
	size_t j;

	if (object_critical(string, "String"))
		return ;
	if (string->priv->is_const == true)
		string_set(string, string->priv->str);
	len = string->priv->len;
	str = string->priv->str;
	i = 0;
	j = len - 1;
	while (i < len && ft_isspace(str[i]))
		++i;
	while (j > i && ft_isspace(str[j]))
		--j;
	if (i > 0 || j < len - 1) {
		len = j - i + 1;
		ft_memmove(str, str + i, len);
		str[len] = '\0';
		string->priv->len = len;
	}
}
