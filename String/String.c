#include "String.h"

typedef struct s_StringPriv
{
	char	*str;
	size_t	len;
	size_t	len_max;
	bool	is_const;
}			t_StringPriv;

static void	string_destroy(void *obj)
{
	String	*string;

	string = obj;
	if (string->priv->is_const == false)
		free(string->priv->str);
}


static String* to_string(void *obj) {
	String* self = dynamic_cast(obj, "String");
	return ref(self);
}
// Constructor

String*	string_copy(String *string) {
	String	*new;

	new = object("String", NULL, string_destroy, sizeof(String)
			+ sizeof(t_StringPriv));
	if (new)
	{
		new->priv = (t_StringPriv *)((u_int8_t *)new + sizeof(String));
		new->priv->len = string->priv->len;
		new->priv->len_max = string->priv->len_max;
		new->priv->str = ft_strdup(string->priv->str);
		if (!new->priv->str)
		{
			unref(new);
			return (NULL);
		}
	}
	return (new);
}

#include "printf.h"
#include <stdio.h>

String	*new_String_format(const char *format, ...) {
	String	*new;
	Object	*obj;
	va_list args;
	size_t len_total;
	size_t len;

	va_start(args, format);
	len_total = (size_t)ft_vsprintf(NULL, format, args);
	va_end(args);
	printf("Len Total: %zu\n", len_total);


	new = object("String", NULL, string_destroy, sizeof(String)
			+ sizeof(t_StringPriv));
	obj = super(new);
	obj->to_string = to_string;
	if (new)
	{
		len = ft_strlen(format);
		printf("%zu et %zu\n", len, len_total);
		new->priv = (t_StringPriv *)((u_int8_t *)new + sizeof(String));
		if (len_total > len) {
			len = len_total;
			new->priv = malloc(sizeof(char) * len);
			va_start(args, format);
			ft_vsprintf(new->priv->str, format, args);
			va_end(args);
			new->priv->is_const = false;
		}
		else {
			new->priv->is_const = true;
			new->priv->str = (char*)format;
		}
		new->priv->len = len;
		new->priv->len_max = len;
	}
	return (new);
}

String	*new_String(const char *str)
{
	String	*new;
	Object	*obj;
	int		len;

	new = object("String", NULL, string_destroy, sizeof(String)
			+ sizeof(t_StringPriv));
	obj = super(new);
	obj->to_string = to_string;
	if (new)
	{
		len = ft_strlen(str);
		new->priv = (t_StringPriv *)((u_int8_t *)new + sizeof(String));
		new->priv->len = len;
		new->priv->len_max = len;
		new->priv->is_const = true;
		new->priv->str = (char*)str;
	}
	return (new);
}

String	*new_String_with_size(char *str, size_t size) {
	String *new;

	new = new_String(str);

	new->priv->is_const = false;
	if (new->priv->len < size)
	{
		new->priv->str = malloc(sizeof(char) * size + 1);
		new->priv->len_max = size;
	}
	else {
		new->priv->str = malloc(sizeof(char) * (new->priv->len + 128));
		new->priv->len_max = new->priv->len;
	}
	ft_memcpy(new->priv->str, str, new->priv->len + 1);
	return new;
}

// join while is not NULL
String	*new_String_join(char *separator, ...)
{
	va_list	args;
	int		count;
	String	*new;
	char	*tmp;
	int		separator_len;

	separator_len = ft_strlen(separator);
	count = 0;
	va_start(args, separator);
	while ((tmp = va_arg(args, char *)) != NULL)
		count += ft_strlen(tmp) + separator_len;
	va_end(args);
	new = new_String_with_size("", count);
	va_start(args, separator);
	while ((tmp = va_arg(args, char *)) != NULL)
	{
		string_append(new, tmp);
		string_append(new, separator);
	}
	new->priv->str[new->priv->len - separator_len] = '\0';
	new->priv->len -= separator_len;
	va_end(args);
	return (new);
}

inline char	*string_get(String *string)
{
	if (object_critical(string, "String"))
		return (NULL);
	return (string->priv->str);
}

inline int	string_len(String *string)
{
	if (object_critical(string, "String"))
		return (-1);
	return (string->priv->len);
}
