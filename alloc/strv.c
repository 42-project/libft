/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strv.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nda-cunh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/08/24 23:07:14 by nda-cunh          #+#    #+#             */
/*   Updated: 2024/09/25 00:56:33 by nda-cunh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_uint	strlenv(char **str_array)
{
	t_uint	i;

	i = 0;
	while (str_array[i] != NULL)
		++i;
	return (i);
}

bool	strv_contains(char **str_array, char *find)
{
	size_t	i;

	i = 0;
	while (str_array[i] != NULL)
	{
		if (ft_strcmp(str_array[i], find) == 0)
			return (true);
		++i;
	}
	return (false);
}

char	**strdupv(char **str_array)
{
	char	**new;
	size_t	array_len;
	size_t	i;

	array_len = strlenv(str_array);
	new = ft_calloc(sizeof(void *), array_len);
	if (new == NULL)
		return (NULL);
	i = 0;
	while (i != array_len)
	{
		new[i] = ft_strdup(str_array[i]);
		if (new[i] == NULL)
		{
			freev(new);
			break ;
		}
		++i;
	}
	return (new);
}
