/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strdup.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nda-cunh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/26 13:27:13 by nda-cunh          #+#    #+#             */
/*   Updated: 2024/09/25 03:08:31 by nda-cunh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup_len(const char *src, size_t *len)
{
	char	*dest;

	*len = ft_strlen(src);
	dest = malloc((*len + 1) * sizeof(char));
	if (!dest)
		return (NULL);
	ft_memcpy(dest, src, (*len + 1));
	return (dest);
}

char	*ft_strdup(const char *src)
{
	size_t	len;
	char	*dest;

	dest = ft_strdup_len(src, &len);
	return (dest);
}
