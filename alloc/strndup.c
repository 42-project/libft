/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strndup.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nda-cunh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/08/24 22:49:43 by nda-cunh          #+#    #+#             */
/*   Updated: 2024/08/24 23:02:23 by nda-cunh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strndup(char *src, size_t n)
{
	char	*dest;

	dest = malloc(n + 1 * sizeof(char));
	if (!dest)
		return (NULL);
	ft_memcpy(dest, src, n);
	dest[n] = '\0';
	return (dest);
}
