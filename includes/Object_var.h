/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Object_var.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nda-cunh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/08/29 00:10:41 by nda-cunh          #+#    #+#             */
/*   Updated: 2024/08/30 22:17:04 by nda-cunh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OBJECT_VAR_H
# define OBJECT_VAR_H

# include "Object.h"

static inline void	_raii_unref(void *obj)
{
	obj = unref(*((Object **)obj));
}

//=============================================================================
//                              Raii Parts
//=============================================================================

# define typename(self) ((Object *)self)->base.type

# define var __attribute__((cleanup(_raii_unref))) __auto_type

#endif
