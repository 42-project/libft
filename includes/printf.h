/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nda-cunh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/18 15:29:50 by nda-cunh          #+#    #+#             */
/*   Updated: 2024/09/03 19:14:37 by nda-cunh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include "libft.h"

int	ft_printf(const char *str, ...);
int	ft_vprintf(const char *str, va_list param);

int	ft_sprintf(char *dest_str, const char *format, ...);
int	ft_vsprintf(char *dest_str, const char *format, va_list param);

#endif
