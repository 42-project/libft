/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nda-cunh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/28 12:46:52 by nda-cunh          #+#    #+#             */
/*   Updated: 2024/10/21 01:22:06 by nda-cunh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include "../char/isalnum.h"
# include "../char/isalpha.h"
# include "../char/isascii.h"
# include "../char/isdigit.h"
# include "../char/isprint.h"
# include "../char/isspace.h"
# include "../char/tolower.h"
# include "../char/toupper.h"
# include <stdarg.h>
# include <stdbool.h>
# include <stdlib.h>
# include <unistd.h>

# define free0(var) (var = (free(var), NULL))

typedef unsigned char	t_uchar;
typedef unsigned int	t_uint;
typedef u_int8_t		t_uint8;
typedef u_int16_t		t_uint16;
typedef u_int32_t		t_uint32;
typedef u_int64_t		t_uint64;
typedef int8_t			t_int8;
typedef int16_t			t_int16;
typedef int32_t			t_int32;
typedef int64_t			t_int64;

void		*ft_calloc(size_t nmemb, size_t size);
char		*ft_strdup(const char *src);
char		*ft_strdup_len(const char *src, size_t *len);
char		*ft_strndup(char *src, size_t n);
bool		has_suffix(char *str, char *suffix);
bool		has_prefix(char *str, char *prefix);
void		ft_bzero(void *s, size_t n);
int			ft_memcmp(const void *s1, const void *s2, size_t n);
void		*ft_memchr(const void *s, int c, size_t n);
void		*ft_memcpy(void *dest, const void *src, size_t n);
void		*ft_mempcpy(void *dest, const void *src, size_t n);
void		*ft_memset(void *s, int c, size_t n);
int			ft_printf(const char *str, ...);
int			ft_atoi(const char *nptr);
char		*ft_strcat(char *dest, const char *src);
char		*ft_strncat(char *dest, const char *src, size_t n);
char		*ft_strncpy(char *dest, const char *src, size_t n);
char		*ft_strcpy(char *dest, const char *src);
char		*ft_stpcpy(char *dest, const char *src);
bool		has_suffix(char *str, char *suffix);
bool		has_prefix(char *str, char *prefix);
char		*ft_itoa(int n);
char		**ft_split(const char *str, const char *charset);
char		**ft_split_unowned(const char *str, const char *charset);
char		*ft_strchr(const char *s, int c);
char		*ft_strjoin(char const *s1, char const *s2);
size_t		ft_strlen(const char *str);
char		*ft_substr(char const *s, unsigned int start, size_t len);
int			ft_strcmp(const char *s1, const char *s2);
int			ft_strncmp(const char *s1, const char *s2, size_t n);
t_uint		strlenv(char **str_array);
bool		strv_contains(char **str_array, char *find);
char		**strdupv(char **str_array);
void		freev(char **tab_str);
int			ft_is_numeric(char c);
char		*ft_strtok(char *str, const char *delimit);
char		*ft_strstr(const char *str, const char *tofind);
void		*ft_memmove(void *dest, const void *src, size_t n);

#endif
