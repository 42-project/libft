#ifndef OBJECT_CLASS_H
# define OBJECT_CLASS_H

# include "Object.h"


//=============================================================================
//                              Class Parts
//=============================================================================

#define class(Type, Parent, content, init, destroy, ...) \
	typedef struct { \
		content \
	} Type; \
	__VA_ARGS__ \
	static inline void Type##_destroy(Object* self)\
{ \
	Type* this = dynamic_cast(self, #Type); \
	destroy \
}\
static inline Type* new_##Type(void) \
{ \
	Type* this = object(#Type, (void*) Parent, Type##_destroy , sizeof(Type)); \
	init \
	return this; \
}

#endif
