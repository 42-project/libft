/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   String.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nda-cunh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/08/30 23:00:14 by nda-cunh          #+#    #+#             */
/*   Updated: 2024/10/21 01:13:31 by nda-cunh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRING_H
# define STRING_H

# include "libft.h"
# include "Object.h"
# include <stdarg.h>

typedef struct s_StringPriv	t_StringPriv;

typedef struct s_String
{
	t_StringPriv	*priv;
}	String;

String	*new_String_with_size(char *str, size_t size);
String	*new_String_join(char *str, ...);
String	*new_String(const char *str);
String	*new_String_format(const char *str, ...);
void	string_append(String *string, char *str_a);
void	string_set(String *string, const char *str);
char	*string_get(String *string);
int		string_len(String *string);
void	string_up(String *string);
void	string_down(String *string);
bool	string_contains(String *string, char *str);
void	string_strip(String *strip);

#endif
