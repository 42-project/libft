#ifndef OBJECT_H
# define OBJECT_H

# include "libft.h"

# define CRITICAL "\033[35;1m[ Critical ]\033[0m "

typedef struct s_Object		Object;
typedef struct s_TypeObject	TypeObject;
typedef struct s_String String;

//=============================================================================
//                            Struct Type and Object
//=============================================================================

typedef struct s_TypeObject
{
	char					*type;
	char					*mem;
	void					(*destroy_func)(void *);
	Object					*parent;
}							TypeObject;

typedef struct s_Object
{
	int						ref_count;
	String					*(*to_string)(void *);
	void					*child;
}							Object;

//=============================================================================
//                             Functions
//=============================================================================

/**
 * @brief Cast the object to the specified type.
 *
 * @param obj The object to cast.
 * @param type_name The name of the object to cast.
 *
 * @return A pointer to the object.
 */
void						*dynamic_cast(void *obj, char *type_name);

/**
 * @brief Reference the object.
 *
 * @param obj The object to reference.
 *
 * @return A pointer to the object.
 */

void						*ref(void *obj);
/**
 * @brief Unreference the object.
 *
 * @param obj The object to unreference.
 *
 * @return A pointer to the object.
 */
void						*unref(void *obj);

/**
 * @brief Check if the object is of the specified type.
 *
 * @param obj The object to check.
 * @param name The name of the object to check.
 *
 * @return true if the object is of the specified type, false otherwise.
 */
bool						object_is(void *obj, char *name);

/**
 * @brief Create a new dynamic object.
 *
 * @param name The name of the object to create. (Foo)
 *
 * @param parent The parent of the object. (Can be NULL)
 *
 * @param destroy The function to call when the object is freed. (Can be NULL)
 *
 * @param size The size of the object to create. sizeof(Foo)
 *
 * @return A void pointer `*` to the newly created object.
 */
void						*object(char *name, void *parent,
								void (*destroy)(void *), size_t size);

/**
 * @brief Check if the object is null.
 *
 * @param obj The object to check.
 * @param name The name of the object to check.
 *
 * @return true if the object is null, false otherwise.
 */
bool						object_critical(void *obj, char *name);

/**
 * @brief Free the object.
 *
 * @param object The object to free.
 */
void						object_free(void *object);

/**
 * @brief Create a shared pointer.
 *
 * @param size The size of the shared pointer.
 *
 * @return A void pointer `*` to the shared pointer.
 */
void*						shared_ptr(size_t size);

//=============================================================================
//							Inline Functions
//=============================================================================

static inline TypeObject	*get_base(Object *obj)
{
	TypeObject	*base;

	base = (TypeObject *)((u_int8_t *)obj - sizeof(TypeObject));
	return (base);
}

static inline void			*super(void *obj)
{
	TypeObject	*base;

	base = (TypeObject *)((u_int8_t *)obj - sizeof(TypeObject));
	return (base->parent);
}

#endif
