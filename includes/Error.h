#ifndef ERROR_H
# define ERROR_H

# include "String.h"

typedef struct Error
{
	int		code;
	String	*message;
}			Error;

Error *new_Error(int code, const char *message);

#endif
