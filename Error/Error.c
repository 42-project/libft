#include "Error.h"

static void	error_destroy(void *error)
{
	Error	*err;

	err = (Error *)error;
	unref(err->message);
}

String* to_string(void *obj)
{
	Error	*error;

	error = dynamic_cast(obj, "Error");
	return (ref(error->message));
}

Error	*new_Error(int code, const char *message)
{
	Error	*error;
	Object	*obj;

	error = object("Error", NULL, error_destroy, sizeof(Error));
	if (error == NULL)
		return (NULL);
	obj = super(error);
	obj->to_string = to_string;
	error->code = code;
	error->message = new_String(message);
	if (error->message == NULL)
		error = unref(error);
	return (error);
}
