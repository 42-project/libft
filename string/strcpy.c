/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strcpy.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nda-cunh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/08/24 23:06:46 by nda-cunh          #+#    #+#             */
/*   Updated: 2024/10/08 00:27:03 by nda-cunh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strcpy(char *dest, const char *src)
{
	size_t	src_len;

	src_len = ft_strlen(src);
	ft_memcpy(dest, src, src_len + 1);
	return (dest);
}

char	*ft_strncpy(char *dest, const char *src, size_t n)
{
	size_t	i;

	i = ft_strlen(src);
	if (i > n)
	{
		i = n;
	}
	ft_memcpy(dest, src, i);
	ft_bzero(dest + i, n - i);
	return (dest);
}

char	*ft_stpcpy(char *dest, const char *src)
{
	size_t	src_len;

	src_len = ft_strlen(src);
	ft_memcpy(dest, src, src_len + 1);
	return (&dest[src_len + 1]);
}
