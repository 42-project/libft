#include "libft.h"

size_t	ft_strlcpy(char *dest, char *src, size_t len)
{
	size_t	i;
	size_t	src_len;

	i = 0;
	src_len = ft_strlen(src);
	if (src_len > len)
		src_len = len;
	ft_memcpy(dest, src, src_len);
	dest[src_len] = '\0';
	return (src_len);
}
