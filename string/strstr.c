#include "libft.h"

char	*ft_strstr(const char *str, const char *tofind)
{
	int	len_tofind;
	int	len_str;
	int	i;

	if (tofind[0] == '\0')
		return ((char *)str);
	len_tofind = ft_strlen(tofind);
	len_str = ft_strlen(str);
	i = 0;
	while (len_str >= i + len_tofind)
	{
		if (ft_memcmp(&str[i], tofind, len_tofind) == 0)
			return ((char *)(&str[i]));
		++i;
	}
	return (NULL);
}
