#include "libft.h"

size_t	ft_strlcat(char *dest, char *src, size_t len)
{
	size_t	len_dest;
	size_t	len_src;

	len_dest = ft_strlen(dest);
	len_src = ft_strlen(src);
	if (len < (len_src + len_dest))
		len_src = len - len_dest;
	ft_memcpy(dest + len_dest, src, len_src);
	dest[len_dest + len_src] = '\0';
	return (len_dest + len_src);
}
