/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   itoa.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nda-cunh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/30 20:05:02 by nda-cunh          #+#    #+#             */
/*   Updated: 2024/10/03 22:59:11 by nda-cunh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

inline static int	_is_negative(long *nbr)
{
	if (0 > *nbr)
	{
		*nbr = -*nbr;
		return (1);
	}
	return (0);
}

char	*ft_itoa(int n)
{
	char	*str;
	long	nbr;
	int		neg;
	int		i;

	i = 11;
	nbr = n;
	neg = _is_negative((long *)&nbr);
	str = malloc(12 * sizeof(char));
	if (!str)
		return (NULL);
	while (nbr >= 10)
	{
		str[i] = nbr % 10 + '0';
		nbr = nbr / 10;
		--i;
	}
	str[i] = nbr % 10 + '0';
	if (neg == 1)
		str[--i] = '-';
	ft_memcpy(str, str + i, 12 - i);
	str[12 - i] = '\0';
	return (str);
}
