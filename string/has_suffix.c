/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   has_suffix.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nda-cunh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/08/24 23:07:46 by nda-cunh          #+#    #+#             */
/*   Updated: 2024/08/30 03:48:33 by nda-cunh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

bool	has_suffix(char* str, char* suffix)
{
	int	len_str;
	int	len_suffix;
	int	offset;

	if (str == NULL || suffix == NULL)
		return (false);
	len_str = ft_strlen(str);
	len_suffix = ft_strlen(suffix);
	if (len_suffix > len_str)
		return (false);
	offset = len_str - len_suffix;
	return (ft_memcmp(&str[offset], suffix, len_suffix) == 0);
}

bool	has_prefix(char* str, char* prefix)
{
	int	len_str;
	int	len_prefix;

	if (str == NULL || prefix == NULL)
		return (false);
	len_str = ft_strlen(str);
	len_prefix = ft_strlen(prefix);
	if (len_prefix > len_str)
		return (false);
	return (ft_memcmp(str, prefix, len_prefix) == 0);
}
