/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strcmp.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nda-cunh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/08/24 23:06:34 by nda-cunh          #+#    #+#             */
/*   Updated: 2024/10/04 00:17:05 by nda-cunh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strcmp(const char *s1, const char *s2)
{
	int	len_s1;
	int	len_s2;
	int	max;

	len_s1 = ft_strlen(s1);
	len_s2 = ft_strlen(s2);
	if (len_s1 >= len_s2)
		max = len_s2;
	else
		max = len_s1;
	return (ft_memcmp(s1, s2, max));
}

int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	size_t	len_s1;
	size_t	len_s2;
	size_t	max;
	if (n == 0)
		return (0);
	len_s1 = ft_strlen(s1);
	len_s2 = ft_strlen(s2);
	if (len_s1 >= len_s2)
		max = len_s1;
	else
		max = len_s2;
	if (n < max)
		max = n;
	return (ft_memcmp(s1, s2, max));
}
