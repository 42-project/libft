#include "libft.h"

char	*ft_strtrim(char const *s1, char const *set)
{
	char	*begin;
	char	*end;

	if (!s1 || !set)
		return (ft_strndup("", 1));
	begin = (char *)s1;
	end = (char *)s1 + ft_strlen(s1) - 1;
	while (ft_strchr(set, *begin))
		begin++;
	while (ft_strchr(set, *end))
		end--;
	if (begin > end)
		return (ft_strndup("", 1));
	return (ft_strndup(begin, (end - begin) + 1));
}
