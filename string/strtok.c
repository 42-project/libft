#include "libft.h"

char	*ft_strtok(char *str, const char *delimit)
{
	static char	*s = NULL;
	char		*unowned;
	char		*result;

	if (str != NULL)
		s = str;
	if (s == NULL)
		return (NULL);
	unowned = ft_strstr(s, delimit);
	if (unowned == NULL)
	{
		result = s;
		s = NULL;
		return (result);
	}
	unowned[0] = '\0';
	result = s;
	s = unowned + ft_strlen(delimit);
	return (result);
}
