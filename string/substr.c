/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   substr.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nda-cunh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/30 13:49:46 by nda-cunh          #+#    #+#             */
/*   Updated: 2024/10/21 01:25:10 by nda-cunh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	size_t	s_len;
	size_t	len_alloc;
	char	*result;

	if (s == NULL)
		return (NULL);
	s_len = ft_strlen(s);
	len_alloc = len;
	if (s_len - start < len_alloc)
		len_alloc = s_len - start;
	result = malloc(sizeof(char) * (len_alloc + 1));
	if (result == NULL)
		return NULL;
	if (start > s_len)
		start = s_len;
	ft_memcpy(result, s + start, len_alloc);
	result[len_alloc] = '\0';
	return (result);
}
