#include "libft.h"

static char	**split_alloc(const char *str, const char *charset, int bonus_len,
		int *count)
{
	int		charset_len;
	char	**result;
	char	*unowned;

	unowned = (char *)str;
	charset_len = ft_strlen(charset);
	*count = 0;
	while (unowned != NULL)
	{
		unowned = ft_strstr(unowned, charset) + charset_len;
		*count = *count + 1;
	}
	result = malloc(sizeof(void *) * (*count + 1) + bonus_len);
	return (result);
}

char	**ft_split_unowned(const char *str, const char *charset)
{
	char	**result;
	char	*unowned;
	char	*dup;
	size_t	dup_len;
	int		count;

	if (str == NULL || charset == NULL)
		return (NULL);
	dup_len = ft_strlen(str);
	result = split_alloc(str, charset, dup_len + 1, &count);
	if (result == NULL)
		return (NULL);
	dup = (char *)result + (sizeof(void *) * (count + 1));
	ft_memcpy(dup, str, dup_len + 1);
	unowned = ft_strtok(dup, charset);
	result[0] = dup;
	count = 1;
	while (unowned != NULL)
	{
		unowned = ft_strtok(NULL, charset);
		result[count] = unowned;
		++count;
	}
	result[0] = dup;
	return (result);
}

char	**ft_split(const char *str, const char *charset)
{
	char	**result;
	int		i;

	result = ft_split_unowned(str, charset);
	if (result == NULL)
		return (NULL);
	i = 0;
	while (result[i] != NULL)
	{
		result[i] = ft_strdup(result[i]);
		if (result[i] == NULL)
		{
			freev(result);
			return (NULL);
		}
		++i;
	}
	return (result);
}
