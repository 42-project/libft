#include "Object.h"

void	*ref(void *obj)
{
	Object	*it;

	if (obj == NULL)
	{
		ft_printf(CRITICAL "object_ref(NULL)\n");
		return (NULL);
	}
	it = obj;
	while (super(it) != NULL)
		it = super(it);
	it->ref_count += 1;
	return (obj);
}

void	*dynamic_cast(void *obj, char *type_name)
{
	Object	*it;

	it = obj;
	if (type_name != NULL && obj != NULL)
	{
		while (get_base(it)->parent != NULL) {
			it = get_base(it)->parent;
		}
		it = it->child;


		while (get_base(it)->parent != NULL)
		{
			if (ft_strcmp(get_base(it)->type, type_name) == 0)
				return (it);
			it = get_base(it)->parent;
		}
		if (ft_strcmp(get_base(it)->type, type_name) == 0)
			return (it);
	}
	if (obj == NULL)
		ft_printf(CRITICAL " (object) is null\n");
	else
		ft_printf(CRITICAL " base (%s) not exist in object (%s)\n", type_name,
	get_base(obj)->type);
	return (NULL);
}

void	*unref(void *obj)
{
	Object	*it;
	Object	*tmp;
	TypeObject *base;

	if (obj == NULL)
		return (NULL);
	it = (Object*)obj;
	while (get_base(it)->parent != NULL)
		it = get_base(it)->parent;
	it->ref_count--;
	if (it->ref_count == 0)
	{
		it = it->child;
		while (get_base(it)->parent != NULL)
		{
			base = get_base(it);
			if (base->destroy_func != NULL)
				base->destroy_func(it);	
			tmp = base->parent;
			if (base->mem != NULL)
				free(base->mem);
			it = tmp;
		}
		base = get_base(it);
		if (base->destroy_func != NULL)
			base->destroy_func(it);	
		if (base->mem != NULL)
			free(base->mem);
		return (NULL);
	}
	return (obj);
}

bool	object_is(void *obj, char *name)
{
	TypeObject	*base;

	if (name == NULL)
		return (false);
	base = get_base(obj);
	return ((ft_strcmp(base->type, name) == 0));
}

bool	object_critical(void *obj, char *name)
{
	TypeObject	*base;

	if (name == NULL || obj == NULL)
	{
		ft_printf(CRITICAL " object_critical(NULL, %s)\n", name);
		return (true);
	}
	base = get_base(obj);
	if (ft_strcmp(base->type, name) != 0)
	{
		ft_printf(CRITICAL " dynamic_cast(%s, %s)\n", base->type, name);
		return (true);
	}
	return (false);
}
