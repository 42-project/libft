#include "Object.h"

static Object* object_new(void* new)
{
	TypeObject	*base;
	Object		*obj;

	base = (TypeObject *)(new);
	obj = (Object *)((u_int8_t *)new + sizeof(TypeObject));
	base->mem = new;
	obj->ref_count = 1;
	base->type = "Object";
	return (obj);
}

void	*object(char *name, void *parent, void (*destroy)(void *),
									size_t size_type)
{
	void		*new;
	TypeObject	*base;
	Object		*obj;

	if (name == NULL)
		return (NULL);
	if (parent == NULL) {
		new = ft_calloc(sizeof(TypeObject) + sizeof(Object)    +    sizeof(TypeObject) + size_type, 1);
		object_new(new);
		obj = (Object*)((u_int8_t*)new + sizeof(TypeObject));
	
		base = (TypeObject *)((u_int8_t *)obj + sizeof(Object));
		base->mem = NULL;
		base->parent = obj;
		base->type = name;
		base->destroy_func = destroy;

		new = (void *)((u_int8_t *)base + sizeof(TypeObject));
		obj->child = new; 
		return (new);
	}
	else {
		new = calloc(sizeof(TypeObject) + size_type, 1);
		if (new == NULL)
		{
			unref(parent);
			return (NULL);
		}

		base = (TypeObject *)(new);
		base->mem = new;
		base->destroy_func = destroy;
		base->parent = parent;
		base->type = name;

		obj = dynamic_cast(parent, "Object");
		obj->child = (u_int8_t *)new + sizeof(TypeObject);
		return (obj->child);

	}
}

void* shared_ptr(size_t size)
{
	return (object("SimpleType", NULL, NULL, size));
}

void	object_free(void *object)
{
	Object	*obj;

	obj = dynamic_cast(object, "Object");
	obj->ref_count = 1;
	unref(object);
}
