CC = gcc -g
NAME = libft.a
SRCS = alloc/free.c alloc/strndup.c alloc/strv.c alloc/calloc.c alloc/strdup.c memory/bzero.c memory/memchr.c memory/memcmp.c memory/memcpy.c memory/memset.c printf/ft_printf.c printf/ft_printf_c.c printf/ft_printf_d.c printf/ft_printf_i.c printf/ft_printf_p.c printf/ft_printf_s.c printf/ft_printf_u.c printf/ft_printf_x.c printf/ft_printf_X.c string/atoi.c string/has_suffix.c string/strstr.c string/strcpy.c string/strcat.c string/itoa.c string/split.c string/strchr.c string/strcmp.c string/strjoin.c string/strlen.c string/substr.c string/strtok.c string/strtrim.c
CFLAGS = -Wall -Wextra -O2 
OBJS = $(SRCS:.c=.o) $(PRINTF_SRC:.c=.o)


all: $(NAME) 

$(NAME): 
	ninja -C build install

so: 	# gcc --shared $(OBJS) -o libft.so 
	ninja -C build install

.c.o: $(SRCS)
	${CC} ${CFLAGS} -c $< -o $@

clean:
	rm -rf $(OBJS)

fclean: clean
	rm -rf $(NAME)

re: fclean all

run: main.c 
	ninja install -C build  
	gcc -g main.c -I ./includes -o main $(NAME)
	./main

run2: main.c 
	ninja install -C build  
	gcc -g main.c -I ./includes -o main $(NAME)
	valgrind ./main

.PHONY: clean fclean re all
