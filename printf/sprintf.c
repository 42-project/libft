#include "printf.h"

int	percent_print(char *dest, const char **format, va_list param);
int	ft_sprintf_count(const char *format, va_list param);

int	ft_vsprintf(char *dest_str, const char *format, va_list param)
{
	int		res;
	char	*end;
	int		len;

	res = 0;
	if (dest_str == NULL)
		return (ft_sprintf_count(format, param));
	while (true)
	{
		end = ft_strchr(format, '%');
		if (end == NULL)
		{
			len = ft_strlen(format);
			ft_memcpy(dest_str + res, format, len);
			return (res + len);
		}
		len = end - format;
		ft_memcpy(dest_str + res, format, len);
		res += len;
		format = end + 1;
		res += percent_print(dest_str + res, &format, param);
		format++;
	}
}

int	ft_sprintf(char *dest_str, const char *format, ...)
{
	va_list	param;
	int		res;

	va_start(param, format);
	res = ft_vsprintf(dest_str, format, param);
	va_end(param);
	return (res);
}
