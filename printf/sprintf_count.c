#include "printf.h"
#include "libft.h"
#include <aio.h>
#include <stddef.h>

static int	option_s_count(va_list param)
{
	char	*str;

	str = va_arg(param, char *);
	if (str == NULL)
		return (6);
	return (ft_strlen(str));
}

static int ft_numlen_unsigned(size_t num, int base_len)
{
    int res;

    res = 1;
    while (num >= (size_t)base_len) {
        num /= base_len;
        ++res;
    }
    return res;
}

static int	ft_numlen_signed(ssize_t num, int base)
{
	int	res;

	res = 0;
	if (num < 0)
	{
		++res;
		num = -num;
	}
	return (res + ft_numlen_unsigned(num, base)); 
}

static int	letter_count(const char *format, va_list param)
{
	if (format[0] == 's')
		return (option_s_count(param) + 1);
	else if (format[0] == 'd' || format[0] == 'i')
		return (ft_numlen_signed(va_arg(param, int), 10));
	else if (format[0] == 'x' || format[0] == 'X')
		return (ft_numlen_unsigned(va_arg(param, size_t), 16));
	else if (format[0] == 'p')
		return (ft_numlen_unsigned(va_arg(param, size_t), 16) + 2);
	else if (format[0] == 'c')
	{
		va_arg(param, int);
		return (1);
	}
	else if (format[0] == 'z' && (format)[1] == 'u')
		return (ft_numlen_unsigned(va_arg(param, size_t), 10) - 1);
	else
		return (-1);
}

int	ft_sprintf_count(const char *format, va_list param)
{
	int	res;

	res = ft_strlen(format);
	while (true)
	{
		format = ft_strchr(format, '%');
		if (format == NULL)
			return (res);
		++format;
		res += letter_count(format, param);
		++format;
	}
	va_end(param);
}
