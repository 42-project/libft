#include "printf.h"
#include "String.h"

int	ft_sprintf_count(const char *format, va_list param);

static int	ft_printf_object(va_list param)
{
	Object	*obj;
	void	*ptr;
	String	*str;
	int		len;
	char	*s;

	ptr = va_arg(param, void *);
	obj = dynamic_cast(ptr, "Object");
	if (obj == NULL || obj->to_string == NULL)
		return (write(1, "(null)", 6));
	str = obj->to_string(obj);
	len = string_len(str);
	s = string_get(str);
	if (s == NULL)
		len = write(1, "(null)", 6);
	else
		len = write(1, s, string_len(str));
	unref(str);
	return (len);
}

typedef struct s_Buffer
{
	char		*buffer;
	size_t			len;
	size_t			len_max;
}				t_Buffer;

static t_Buffer	*get_buffer(void)
{
	static t_Buffer	buffer = (t_Buffer){NULL, 0, 0};

	if (buffer.buffer == NULL)
	{
		buffer.len = 0;
		buffer.len_max = 65536;
		buffer.buffer = malloc(buffer.len_max);
	}
	return (&buffer);
}

void	ft_fflush(void)
{
	t_Buffer	*buffer;

	buffer = get_buffer();
	write(1, buffer->buffer, buffer->len);
	buffer->len = 0;
}

__attribute__((destructor))
static void	free_buffer(void)
{
	t_Buffer	*buffer;

	buffer = get_buffer();
	ft_fflush();
	free(buffer->buffer);
}

int	ft_vprintf(const char *str, va_list param)
{
	va_list		param2;
	t_Buffer	*buffer;
	size_t		res;
	int			count;

	va_copy(param2, param);
	buffer = get_buffer();
	count = ft_sprintf_count(str, param);
	if (count >= buffer->len_max)
	{
		buffer->len_max = count + 1;
		free(buffer->buffer);
		buffer->buffer = malloc(buffer->len_max);
	}
	if (buffer->len + count >= buffer->len_max)
	{
		ft_fflush();
		buffer->len = 0;
	}
	res = ft_vsprintf(buffer->buffer + buffer->len, str, param2);
	if (res != 0) {
		buffer->len += res;
		if (buffer->buffer[buffer->len - 1] == '\n')
			ft_fflush();
	}
	return (res);
}

int	ft_printf(const char *str, ...)
{
	va_list		param;
	int			res;

	va_start(param, str);
	res = ft_vprintf(str, param);
	ft_fflush();
	va_end(param);
	return (res);
}
