#include "includes/printf.h"

static int	percent_s(char *dest, const char *str)
{
	int	len;

	if (str == NULL)
	{
		ft_memcpy(dest, "(null)", 6);
		return (6);
	}
	len = ft_strlen(str);
	ft_memcpy(dest, str, len);
	return (len);
}

static int	percent_c(char *dest, int c)
{
	ft_memcpy(dest, &c, 1);
	return (1);
}

static int	putnbr_base(char *dest, const char *base, size_t nb)
{
	char	buffer[64];
	char	*end;
	size_t	base_len;

	end = buffer + 63;
	base_len = ft_strlen(base);
	*end-- = '\0';
	if (nb == 0)
	{
		*end = base[0];
		ft_memcpy(dest, end, buffer + 63 - end);
		return (buffer + 63 - end);
	}
	while (nb)
	{
		*end-- = base[nb % base_len];
		nb /= base_len;
	}
	ft_memcpy(dest, end + 1, buffer + 63 - end);
	return (buffer + 62 - end);
}

static int	sputnbr_base(char *dest, const char *base, ssize_t nb)
{
	if (nb < 0)
	{
		dest[0] = '-';
		return (1 + putnbr_base(dest + 1, base, -nb));
	}
	return (putnbr_base(dest, base, nb));
}

int	percent_print(char *dest, const char **format, va_list param)
{
	if (*format[0] == 's')
		return percent_s(dest, va_arg(param, const char *));
	if (*format[0] == 'd' || *format[0] == 'i')
		return sputnbr_base(dest, "0123456789", va_arg(param, int));
	if (*format[0] == 'u')
		return sputnbr_base(dest, "0123456789", va_arg(param, unsigned int));
	if (*format[0] == 'x')
		return sputnbr_base(dest, "0123456789abcdef", va_arg(param, size_t));
	if (*format[0] == 'X')
		return sputnbr_base(dest, "0123456789ABCDEF", va_arg(param, size_t));
	if ((*format[0]) == 'z' && (*format)[1] == 'u')
		return putnbr_base(dest, "0123456789", va_arg(param, size_t));
	if (*format[0] == 'c')
		return percent_c(dest, va_arg(param, int));
	if (*format[0] == 'p')
	{
		size_t p = va_arg(param, size_t); 
		if (p == 0)
		{
			dest = ft_mempcpy(dest, "(nil)", 5);
			return (5);
		}
		dest = ft_mempcpy(dest, "0x", 2);
		return (2 + putnbr_base(dest, "0123456789abcdef", p));
	}
	else 
		dest[0] = '%';
	return 1;
}
