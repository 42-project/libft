#ifndef SIGNAL_H
# define SIGNAL_H

#include "Object.h"

typedef struct ArrayPriv ArrayPriv; 

typedef struct Array 
{
	ArrayPriv *priv;
}	Array;

Array* new_Array(size_t size_of_element, void (*free_element)(void*));

#endif
