#include "Array.h"

typedef struct ArrayPriv 
{
	size_t size_of_element;
	void (*free_element)(void*);
	void* data;
}	ArrayPriv;

Array* new_Array(size_t size_of_element, void (*free_element)(void*)) {
	Array* new = object("Array", NULL, NULL, sizeof(Array) + sizeof(ArrayPriv));
	if (new == NULL)
		return NULL;
	new->priv = (ArrayPriv*)((char*)new + sizeof(Array));
	new->priv->size_of_element = size_of_element;
	new->priv->free_element = free_element;
	return new;
}

