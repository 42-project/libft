#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include "Object_var.h"
#include "Array/Array.h"
#include "String.h"


typedef struct Personnage {
	int vie;
	String* name;
} Personnage;

static String* to_string(void *self)
{
	Personnage* mario = dynamic_cast(self, "Personnage");
	return ref(mario->name);
}

static void personnage_destroy(void *self) {
	Personnage* perso = self;
	ft_printf("\033[95m[Personnage] \033[0ml'object (%S) est detruit:(\n", perso->name);
	unref(perso->name);
}

Personnage* new_Personnage(int vie, const char* name)
{
	Personnage* mario = object("Personnage", NULL, personnage_destroy, sizeof(Personnage));
	Object* obj = super(mario);
	obj->to_string = to_string;
	mario->vie = vie;
	mario->name = new_String(name);
	return mario;
}

# include <time.h>

int	ft_sprintf(char *dest_str, const char *format, ...);

#include "printf.h"

void ft_debug (char *str, ...) {
	va_list args;

	va_start(args, str);
	ft_vprintf(str, args);
	va_end(args);
}

int ft_magik (char *str, ...) {
	va_list args;

	va_start(args, str);
	int n = ft_vsprintf(NULL, str, args);
	va_end(args);
	return n;
}

#include <limits.h>
#include "includes/printf.h"

int	main()
{
	var str =  new_String("Hello World");


	return  0;
}
