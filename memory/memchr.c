/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memchr.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nda-cunh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/08/24 22:32:50 by nda-cunh          #+#    #+#             */
/*   Updated: 2024/09/25 00:48:28 by nda-cunh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	t_uchar	*str;
	t_uchar	c_tmp;

	c_tmp = (unsigned char)c;
	str = (unsigned char *)s;
	++n;
	while (--n)
	{
		if (*str == c_tmp)
			return (str);
		++str;
	}
	return (NULL);
}
