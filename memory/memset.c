/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memset.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nda-cunh <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/28 15:46:08 by nda-cunh          #+#    #+#             */
/*   Updated: 2024/09/25 00:49:40 by nda-cunh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

inline static size_t make_size_t(t_uchar byte) {
	size_t result;
	t_uchar *ptr;

	result = 0;
	ptr = (t_uchar*)&result;
	ptr[0] = byte;
	ptr[1] = byte;
	ptr[2] = byte;
	ptr[3] = byte;
	ptr[4] = byte;
	ptr[5] = byte;
	ptr[6] = byte;
	ptr[7] = byte;
	return result;
}

void	*ft_memset(void *s, int c, size_t n)
{
	size_t byte = make_size_t((t_uchar)c);
	size_t *ptr;
	t_uchar *ptr_c;

	ptr = s;
	while (n >= sizeof(size_t))
	{
		*ptr = byte;
		++ptr;
		n -= sizeof(size_t);
	}
	ptr_c = (t_uchar*)ptr;
	while (n > 0) {
		*ptr_c = c;
		++ptr_c;
		n--;
	}
	return (s);
}
