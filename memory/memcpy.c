#include "libft.h"

void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	size_t *ptr_dst;
	size_t *ptr_src;
	t_uchar *ptr_src_c;
	t_uchar *ptr_dst_c;

	if ((!dest && !src) || n == 0)
		return (dest);
	ptr_dst = (size_t*)dest;
	ptr_src = (size_t*)src;
	while (n >= sizeof(size_t))
	{
		*ptr_dst = *ptr_src;
		++ptr_dst;
		++ptr_src;
		n -= sizeof(size_t);
	}
	ptr_dst_c = (t_uchar*)ptr_dst;
	ptr_src_c = (t_uchar*)ptr_src;
	while (n > 0) {
		*ptr_dst_c = *ptr_src_c;
		++ptr_dst_c;
		++ptr_src_c;
		n--;
	}
	return (dest);
}

void	*ft_mempcpy(void *dest, const void *src, size_t n)
{
	size_t _n = n;
	size_t *ptr_dst;
	size_t *ptr_src;
	t_uchar *ptr_src_c;
	t_uchar *ptr_dst_c;

	if ((!dest && !src) || n == 0)
		return (dest);
	ptr_dst = (size_t*)dest;
	ptr_src = (size_t*)src;
	while (n >= sizeof(size_t))
	{
		*ptr_dst = *ptr_src;
		++ptr_dst;
		++ptr_src;
		n -= sizeof(size_t);
	}
	ptr_dst_c = (t_uchar*)ptr_dst;
	ptr_src_c = (t_uchar*)ptr_src;
	while (n > 0) {
		*ptr_dst_c = *ptr_src_c;
		++ptr_dst_c;
		++ptr_src_c;
		n--;
	}
	return (dest + _n);
}
